# Try to find GLFW3 library and include directories
#
# References:
# - https://gist.github.com/Caellian/9be4a8a617e59ec3eee93b37a9cf19c9
#
# Once done this will define:
# - GLFW_FOUND
# - GLFW_LIBRARIES
# - GLFW_INCLUDE_DIRS

include(FindPackageHandleStandardArgs)

if (NOT UNIX)
    if (NOT GLFW_ROOT)
        message("ERROR: GLFW_ROOT must be set!")
    endif (NOT GLFW_ROOT)

    find_path(GLFW_INCLUDE_DIRS DOC "Path to GLFW include directory."
            NAMES GLFW/glfw3.h
            PATHS ${GLFW_ROOT}/include)

    if (MSVC15)
        find_library(GLFW_LIBRARIES DOC "Absolute path to GLFW library."
                NAMES glfw3.lib
                PATHS ${GLFW_ROOT}/lib-vc2015)
    elseif (MSVC13)
        find_library(GLFW_LIBRARIES DOC "Absolute path to GLFW library."
                NAMES glfw3.lib
                PATHS ${GLFW_ROOT}/lib-vc2013)
    elseif (MSVC12)
        find_library(GLFW_LIBRARIES DOC "Absolute path to GLFW library."
                NAMES glfw3.lib
                PATHS ${GLFW_ROOT}/lib-vc2012)
    elseif (MSVC10)
        find_library(GLFW_LIBRARIES DOC "Absolute path to GLFW library."
                NAMES glfw3.lib
                PATHS ${GLFW_ROOT}/lib-vc2010)
    elseif (MINGW)
        if (CMAKE_CL_64)
            find_library(GLFW_LIBRARIES DOC "Absolute path to GLFW library."
                    NAMES glfw3.dll
                    PATHS ${GLFW_ROOT}/lib-mingw-w64)
        else (CMAKE_CL_64)
            find_library(GLFW_LIBRARIES DOC "Absolute path to GLFW library."
                    NAMES glfw3.dll
                    PATHS ${GLFW_ROOT}/lib-mingw)
        endif (CMAKE_CL_64)
    else (MINGW)
        # Default to latest version of VC libs
        find_library(GLFW_LIBRARIES DOC "Absolute path to GLFW library."
                NAMES glfw3.lib
                PATHS ${GLFW_ROOT}/lib-vc2015)
    endif (MSVC15)
else (NOT UNIX)
    find_path(GLFW_INCLUDE_DIRS DOC "Path to GLFW include directory."
            NAMES GLFW/glfw3.h
            PATHS
            /usr/include
            /usr/local/include
            /usr/target/include
            /sw/include
            /opt/local/include)

    find_library(GLFW_LIBRARIES DOC "Absolute path to GLFW library."
            NAMES libglfw.so
            PATHS
            /usr/local/lib
            /usr/lib
            /lib)
endif (NOT UNIX)

find_package_handle_standard_args(GLFW DEFAULT_MSG GLFW_LIBRARIES GLFW_INCLUDE_DIRS)

mark_as_advanced(GLFW_INCLUDE_DIRS GLFW_LIBRARIES)