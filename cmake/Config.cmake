# Detect target platform and architecture
#
# References:
# - https://gitlab.kitware.com/cmake/community/wikis/doc/tutorials/How-To-Write-Platform-Checks#platform-checking
# - https://cmake.org/cmake/help/v3.15/variable/CMAKE_SIZEOF_VOID_P.html#variable:CMAKE_SIZEOF_VOID_P
#
# Once done this will define:
# - TEKEIMYUNG_TARGET_PLATFORM
# - TEKEIMYUNG_TARGET_ARCH

# Detect target OS platform
if (${CMAKE_SYSTEM_NAME} STREQUAL "Windows")
    set(TEKEIMYUNG_TARGET_PLATFORM "Windows")
elseif (${CMAKE_SYSTEM_NAME} STREQUAL "Linux")
    set(TEKEIMYUNG_TARGET_PLATFORM "Linux")
else()
    message(FATAL_ERROR "Unsupported target OS platform (must be either Windows or Linux)")
endif()

# Define target architecture
if (${CMAKE_SIZEOF_VOID_P} STREQUAL "4")
    # Target architecture is 32 bits
    set(TEKEIMYUNG_TARGET_ARCH "x86")
elseif (${CMAKE_SIZEOF_VOID_P} STREQUAL "8")
    # Target architecture is 64 bits
    set(TEKEIMYUNG_TARGET_ARCH "x64")
endif()
