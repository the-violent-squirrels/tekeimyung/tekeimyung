/**
* @Author   Guillaume Labey
*/

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

#include <iostream>

#include <Engine/BasicState.hpp>
#include <Engine/Core/Engine.hpp>
#include <Engine/EntityFactory.hpp>
#include <Engine/Utils/Exception.hpp>
#include <Engine/Utils/EventSound.hpp>
#include <Engine/Utils/LevelLoader.hpp>
#include <Engine/Debug/Logger.hpp>
#include <Engine/Utils/ResourceManager.hpp>
#include <Engine/Debug/Debug.hpp>

void    windowCloseHandler(void* data)
{
    Engine* engine = static_cast<Engine*>(data);
    auto& gameStateManager = engine->getGameStateManager();

    gameStateManager.clearStates();
}

int     main(int ac, char** av)
{
    Engine engine;
    auto &&gameStateManager = engine.getGameStateManager();
    try
    {
        if (!engine.init())
            return (1);

        // Load textures, models & sounds
        ResourceManager::getInstance()->loadResources("resources");

        // Load geometries: plane, sphere, box, circle
        GeometryFactory::initGeometries();

        // Load entities after engine initialization to have logs
        EntityFactory::loadDirectory(ARCHETYPES_LOCATION);

        //EventSound::loadEvents();
        GameWindow::getInstance()->registerCloseHandler(windowCloseHandler, &engine);

        //  Before, we were playing the PlayState at first, now we have to play the HomeScreenState !
        //  std::shared_ptr<PlayState> playState = std::make_shared<PlayState>(&gameStateManager);
        //
        //  if (!engine.run(ac, av, playState))
        //      return (1);

        std::shared_ptr<EditorState> editorState = std::make_shared<EditorState>(&gameStateManager);

        if (!engine.run(ac, av, editorState))
            return (1);
    }
    catch (const std::exception& e)
    {
        LOG_ERROR("Exception : %s", e.what());
    }

    engine.stop();
    return (0);
}
